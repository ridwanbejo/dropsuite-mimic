#Dropsuite Mimic

A simple utility for finding similar content within a lot o fils in your operating system.

###System Requirements:

* PHP 5.6
* SQLite3
* Built-int PHP Web Server
* OSX or Linux

###Run the web apps:

```
$ ./start-app.sh
```

Then open the web browser and access this URL: http://localhost:8000/web/home

###Example Usage for The CLI tools to be used independently outside the web apps:

```
$ php console/mimic.php --dir ./data/DropsuiteTest --allowed-file-type txt,csv,md,mkd, --max-file-size 6
$ php console/mimic.php --dir ./data/DropsuiteTest --allowed-file-type txt,csv,md,mkd,
$ php console/mimic.php --dir ./data/DropsuiteTest --allowed-file-type txt --max-file-size 12
$ php console/mimic.php --dir ./data/DropsuiteTest-2 --allowed-file-type txt, --max-file-size 6
```

These are screenshot in different angle:

* Analyzing directory DropsuiteTest in web apps

![](https://www.dropbox.com/s/lfhhau1dhzie2v0/Screen%20Shot%202017-09-23%20at%205.47.27%20PM.png)

* Analyzing directory DropsuiteTest-2 in web apps

![](https://www.dropbox.com/s/jpxagyolmlxv7ws/Screen%20Shot%202017-09-23%20at%205.40.17%20PM.png)

* Analyzing directory DropsuiteTest & DropsuiteTest-2 with console apps

![](https://www.dropbox.com/s/ua0i6dg81cgojvm/Screen%20Shot%202017-09-23%20at%205.46.01%20PM.png)