<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dropsuite Mimic</title>

    <!-- Bootstrap core CSS -->
    <link href="http://localhost:8001/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://localhost:8001/css/portfolio-item.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Dropsuite</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Portfolio Item Heading -->
      <h1 class="my-4">Mimic
        <small>A tools for seek the similar content within a lot of files</small>
      </h1>
      <hr/>

      <!-- Portfolio Item Row -->
      <div class="row">

        <div class="col-md-4">
          <p>Fill this form before analyze the directory:</p>

          <form id="mimic-form" method="POST" action="#" class="form">
            <div class="form-group">
              <label for="dir">Directory Path</label>
              <input type="text" class="form-control" id="dir" aria-describedby="dir-help" name="dir" placeholder="Enter directory path...">
              <small id="dir-help" class="form-text text-muted">Choose your directory to be analyzed by mimic by written the path on this field</small>
            </div>
            <div class="form-group">
              <label for="allowed-file-type">Allowed File Type</label>
              <input type="text" class="form-control" id="allowed-file-type" aria-describedby="allowed-file-type-help" name="allowed-file-type" placeholder="Enter allowed file type..">
              <small id="allowed-file-type-help" class="form-text text-muted">You can choose a text file such as txt, mkd, md, csv, et cetera. And you have to add comma in the end of string to analyze the file which don't has an extension.</small>
            </div>

            <div class="form-group">
              <label for="allowed-file-type">Max File Size (bytes)</label>
              <input type="text" class="form-control" id="max-file-size" aria-describedby="max-file-size-help" name="max-file-size" placeholder="Enter max file size..">
              <small id="max-file-size-help" class="form-text text-muted">You can limit the maximum file size or just ignore it</small>
            </div>

            <input class="btn btn-primary" type="Submit" value="Analyze!" />
          </form>

        </div>

        <div id="mimic-result-wrapper" class="col-md-8" style="overflow-y: scroll; height: 400px; background: #BFD7EA;">
          <pre id="mimic-result" style="padding-top:10px;">
Waiting for analysis...
          </pre>
        </div>
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Ridwan Fadjar Septian 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="http://localhost:8001/vendor/jquery/jquery.min.js"></script>
    <script src="http://localhost:8001/vendor/popper/popper.min.js"></script>
    <script src="http://localhost:8001/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){

          $('#mimic-form').submit(function(e){
              e.preventDefault();
              alert("Form submitted!");

              $.ajax({
                      url: "http://localhost:8000/web/home/analyze", 
                      method: "POST",
                      data:$("#mimic-form").serialize(),
                      success: function(result){
                          $("#mimic-result").html(result);

                          var objDiv = document.getElementById("mimic-result-wrapper");
                          objDiv.scrollTop = objDiv.scrollHeight;
                      }
              });
          });
      });
    </script>

  </body>

</html>
