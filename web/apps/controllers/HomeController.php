<?php

namespace Web\Controllers;

require_once("apps/util/SimpleTemplate.php");

class HomeController {

	public $template;

	function __construct()
	{

	}

	function index()
	{
		$template = new \Web\Util\SimpleTemplate();

		$template->render("home.php");
	}

	function analyze()
	{
		// will process the directory and call the cli tools, Dropsuite Mimic, here

		echo "<ore>";
		echo str_replace("\n", "<br/>", shell_exec ("php ../console/mimic.php --dir ".$_POST['dir']." --allowed-file-type ".$_POST['allowed-file-type']));
		echo "</pre>";
	}
}