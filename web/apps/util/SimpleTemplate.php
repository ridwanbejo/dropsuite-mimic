<?php

namespace Web\Util;

class SimpleTemplate {
    protected $template_dir = 'apps/views/';

    protected $vars = array();

    public function __construct($template_dir = null) {
        if ($template_dir !== null) {
            $this->template_dir = $template_dir;
        }
    }

    public function render($template_file) {
        if (file_exists($this->template_dir.$template_file)) {
            include $this->template_dir.$template_file;
        } else {
            echo ('Template file "' . $template_file . '" is not exist in directory "' . $this->template_dir.'"');
        }
    }

    public function __set($name, $value) {
        $this->vars[$name] = $value;
    }

    public function __get($name) {
        return $this->vars[$name];
    }
}