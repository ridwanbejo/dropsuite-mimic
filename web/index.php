<?php


// Constant for this web application
define("URI_CLASS", '2');
define("URI_CLASS_METHOD", '3');
define("URI_CLASS_METHOD_PARAM", 4);
define("CLASS_SUFFIX", "Controller");
define("CONTROLLER_NAMESPACE", "Web\Controllers");

// Parse the URI to be loaded onto Controller

$uri = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

if (!array_key_exists(3, $uri))
{
	$uri[URI_CLASS_METHOD] = "index";
}
else if (array_key_exists(3, $uri) == TRUE && $uri[3] == "")
{
	$uri[URI_CLASS_METHOD] = "index";
}

$class_name_raw = explode("-", str_replace("_", "-", $uri[URI_CLASS]));
$class_name = ucwords( implode("",  array_map(function($word) { return ucwords($word); }, $class_name_raw) ) ).CLASS_SUFFIX;
$class_method_raw = explode("-", str_replace("_", "-", $uri[URI_CLASS_METHOD]));
$class_method = $class_method_raw[0].ucwords( implode("",  array_map(function($word) { return ucwords($word); }, array_slice($class_method_raw, 1) ) ) );
$class_method_param = array_slice($uri, URI_CLASS_METHOD_PARAM);

// echo "<pre>";
// print_r($uri);
// echo $class_name."\n";
// echo $class_method."\n";
// print_r ($class_method_param);
// echo "\n";
// echo "</pre>";

// Route the parsed URL to Controller

require_once ("apps/controllers/HomeController.php");

if (class_exists(CONTROLLER_NAMESPACE.'\\'.$class_name))
{
	$ref = new ReflectionClass(CONTROLLER_NAMESPACE.'\\'.$class_name);
	$obj = $ref->newInstanceWithoutConstructor();
	if (method_exists($obj, $class_method))
	{
		$obj->$class_method();
	}
	else
	{
		echo "<h1>Sorry action not found!</h1>";
	}
}
else
{
	echo "<h1>Sorry page not found!</h1>";
}