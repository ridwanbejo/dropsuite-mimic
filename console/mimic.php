<?php

/*

- read the directory recursively
- store the content file
- perform query with SQL or array operation to find the most similar content within the files
- get the number of similar content within the files
- clean the analyzing history

*/

// GETTING THE PARAMETER

/*

1. filepath directory
2. allowed file type
3. max file size

$ php console/mimic.php --dir ./data/DropsuiteTest --allowed-file-type "csv,txt,mkd,md" --max-file-size 2048
*/

$defined_cli_opts  = array(
    "dir:",
    "allowed-file-type:",
    "max-file-size:",
);

$args = getopt('', $defined_cli_opts);

if (array_key_exists('dir', $args))
{
	$DIR = $args['dir'];
}
else
{
	print ("--dir is required!\n");
	exit();
}

if (array_key_exists('allowed-file-type', $args))
{
	$ALLOWED_FILE_TYPE = explode(",",$args['allowed-file-type']);
}
else
{
	print ("--allowed-file-type is required!\n");
	exit();
}

if (array_key_exists('max-file-size', $args))
{
	$MAX_FILE_SIZE = $args['max-file-size'];
}
else
{
	$MAX_FILE_SIZE = 0;
}

// INITIALIZATION

$db = new SQLite3(realpath('data/data.db'));

if(!$db) {
  echo $db->lastErrorMsg();
} else {
  echo "Open database success...\n";
}


$sql =<<<EOF
      CREATE TABLE IF NOT EXISTS MIMIC_FILES
      (
	      ID INT PRIMARY KEY NOT NULL,
	      FILEPATH TEXT NOT NULL,
	      FILESIZE INT NOT NULL,
	      CONTENT TEXT NOT NULL
      );
EOF;

$ret = $db->exec($sql);
if(!$ret){
  echo $db->lastErrorMsg();
} else {
  echo "Creating table is success...\n";
}

$sql =<<<EOF
      DELETE FROM MIMIC_FILES;
EOF;

$ret = $db->exec($sql);
if(!$ret){
  echo $db->lastErrorMsg();
} else {
  echo "Clearing the cache...\n";
}

// INDEXING THE CONTENT OF FILES on SQLite3

$query = "";
$i = 1;
$di = new RecursiveDirectoryIterator($DIR);
foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
	if (is_file($filename) && in_array ($file->getExtension() , $ALLOWED_FILE_TYPE) ) {
		$file_size = $file->getSize();
	    print ($filename . " - " . $file_size . " bytes. ");
	    if ($file_size > 0)
	    {
	    	if ($MAX_FILE_SIZE > 0)
			{
				if ($file_size <= $MAX_FILE_SIZE)
				{
					print ("Indexing file...\n");
	    
	    			$f = fopen($filename, "r");
		    		$file_content = fread($f, $file_size);
		    		fclose($f);
				}
				else
				{
					print ("Skipping file because greater than max file size...\n");
	    
	    			continue;
				}
			}
			else
			{
				print ("Indexing file...\n");
	    
	    		$f = fopen($filename, "r");
		    	$file_content = fread($f, filesize($filename));
		    	fclose($f);
			}

		    $query .= "INSERT INTO MIMIC_FILES (ID,FILEPATH,FILESIZE,CONTENT)
	      				VALUES (" . $i . ", '" . $filename . "', " . $file_size . ", '" . $db->escapeString($file_content) . "');";

	    }
	    else
	    {
	    	print ("File is not indexed because empty...\n");
	    }

	    $i ++ ;
	}
}

// print ($query);

$ret = $db->exec($query);
if(!$ret) {
  echo $db->lastErrorMsg();
} else {
  echo "Indexing all file content is success...\n";
}

// GET THE RESULT

$sql = <<<EOF
	SELECT UCONTENT,
	       MAX(SIMILAR_COUNT) AS SIMILAR_COUNT
	  FROM (
	           SELECT umf.UCONTENT,
	                  count(mf.CONTENT) AS SIMILAR_COUNT
	             FROM MIMIC_FILES mf
	                  INNER JOIN
	                  (
	                      SELECT DISTINCT (CONTENT) AS UCONTENT
	                        FROM MIMIC_FILES
	                  )
	                  umf ON mf.CONTENT LIKE "%" || umf.UCONTENT || "%"
	            GROUP BY umf.UCONTENT
	       );
;
EOF;

$row = $db->querySingle($sql, true);
print_r($row);

echo "Content: ". $row['UCONTENT'] . "\n";
echo "Similar Count: ". $row['SIMILAR_COUNT'] ."\n";

echo "The script is done!\n";

$db->close();

